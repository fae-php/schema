.. _column-ref:

*******
Columns
*******

Columns are used across schema manager to refer to a field and can be given a number of options. 

Example
#######

.. code-block:: json

  {
    "column_name": {
      "name": "column_name",
      "type": "int",
      "default": null,
      "required": false,
      "notnull": true,
      "length": 128,
      "unsigned": false,
      "precision": 3,
      "scale": 10,
      "fixed": 10,
      "comment": "",
      "depends": {
        "model_name": {
          "column": "column_name",
          "update": "cascade",
          "delete": "restrict"
        }
      },
      "description": ""
    }
  }

Refernece
#########

*object key*
  Column name can be specified as the key of the object

name
  If a name property exists on the object then that should be used in preference

type
  Types_ define what data type to expect in the column

default
  Default value to be set when not defined on a create event, can be ``null``, ``CURRENT_TIMESTAMP``, ``NOW()`` or any ``string``

required
  If a value is required for the column, can be ``true`` or ``false``, if not speficied it will be auto-calculated based on the ``default`` and ``notnull`` settings

notnull
  Whether the column accepts ``null`` values or not, can be ``true`` or ``false``

comment
  Comment is used on the SQL database as a comment for the 

depends
  Column model dependencies

  *object_key*
    The name of the parent model upon which this column depends upon
  
  column
    The column name from the parent that this column depends upon, if not specified this will default to ``id``
  
  update 
    An action to take when the parent model is updated, supported options are ``cascade``, ``restrict`` and ``set null``
  
  delete 
    An action to take when the parent model is updated, supported options are ``cascade``, ``restrict`` and ``set null``

  relates
    An optional value defining the schema name if the parent model is not local to this schema. 

description
  Used to describe the column in GraphQL schema representations


Types
#####

Types accepted as strings are parsed into `DBAL types <https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/types.html>`_.

*column*
  Types in columns can contain a :ref:`column-ref` object

tinyint
  Integer column type, supports ``length`` and ``unsigned``
smallint
  Integer column type, supports ``length`` and ``unsigned``
int
  Integer column type, supports ``length`` and ``unsigned``
integer
  Integer column type, supports ``length`` and ``unsigned``
decimal
  Decimal column type, supports ``length``, ``unsigned`` and ``precision``
float
  Float column type, supports ``length``, ``unsigned`` and ``precision``
uid
  Maps and converts a Globally Unique Identifier
guid
  Maps and converts a Globally Unique Identifier
varchar
  String column type, supports ``length``
text
  String column type, supports ``length``
set
  String column type, supports ``length`` and ``options``. ``options`` provided will restrict what is accepted from the API for this column.
uri
  String column type, supports ``length``. ``unique`` is forced to true for this
bool
  Boolean column type
boolean
  Boolean column type
date
  Datetime column type
datetime
  Datetime column type
datetz
  Datetime with timezone column type
json
  JSON column type, often stored as text on the SQL side, but will be parsed inline for mutations
binary
  Binary column type
ouuid
  Binary ordered UUID column type, see the `ramsey UUID module <https://github.com/ramsey/uuid-doctrine>`_ for more information
uuid
  UUID column type
