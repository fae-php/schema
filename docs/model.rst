.. _model-ref: 

****************
Model definition
****************

Example
#######

.. code-block:: json

  {
    "model_name":
    {
      "table": "table_name",
      "columns": {
        "column_name": {
          "type": "varchar",
          "length": 128,
          "default": null
        }
      },
      "childrenList": ["child_table"],
      "parents": ["parent_table"],
      "remoteParents": ["schema_name.remoteparent_table"]
      "filters": {
        "filter_name": {
          "type": "varchar",
          "length": 128,
          "default": null
        }
      },
      "fields": {
        "field_name": {
          "type": "varchar",
          "length": 128,
          "default": null
        }
      },
      "description": ""
    }
  }

Reference
#########

*object key*
  Model name can be provided as the key to the object

table
  Table name is the name of the model and will override the object key if set

columns
  :ref:`column-ref` is an object containing `Column` references

childrenList
  ``Auto-generated`` - Children list is an array of strings that refer to other model names directly.

parents
  ``Auto-generated`` - Parents is an array of strings that refer to other model names directly

remoteParents
  ``Auto-generated`` - Remote parents is an array of strings that refer to the schema name and model name of a remote model using "." to separate

filters
  Filters define :ref:`column-ref` references that can be used as additional filters on the model in addition to columns

fields
  Fields define :ref:`column-ref` references that can be used as additional returned data when querying the model addition to columns

description
  Text description used in GraphQL schema
