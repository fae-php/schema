.. _schema-ref: 

*****************
Schema definition
*****************

Example
#######

.. code-block:: json

  {
    "schema_name": "..."
    "data": {
      "items": {}
    },
    "mutations": {
      "items": []
    }
  }

Reference
#########

schema_name
  The name of this schema

data
  Data contains all of the standard model definitions in the schema

  items
    Items can be a key-based object where the key is the name of the :ref:`model-ref` OR an array of :ref:`model-ref` where the name of the model is specified within the object


mutations
  Mutations contain GraphQL mutation endpoints

  items
    Items can be a key-based object where the key is the name of the :ref:`mutation-ref` OR an array of :ref:`mutation-ref` where the name of the mutation is specified within the object 
