.. _mutation-ref: 

*******************
Mutation definition
*******************

Example
#######

.. code-block:: json

  {
    "mutation": "mutationName",
    "input": {
      "type": "nameType",
      "columns": 
      {
        "columnName": {
          "type": {
            "type": "subTypeName",
            "columns": {
              "column_name" {
                "type": "text"
              }
            }
          }
        }
      }
    "type":
      "type": "nameType",
      "columns": 
      {
        "columnName": {
          "type": {
            "type": "subTypeName",
            "columns": {
              "column_name" {
                "type": "text"
              }
            }
          }
        }
      }
    },
    "method": "POST",
    "endpoint": "http://exmaple.com/api/v1/mutationName",
    "description": ""
  }

Reference
#########

mutation
  Mutation name is specified within the object the same as

input
  Input defines the `input` type for the mutation and is a type object of column references. Additional fields supported by :ref:`model-ref` are also supported here

  type
    Name of the type is defined in the "type" attribute, if this is not present a name will be auto-generated
  columns
    An array/object of :ref:`column-ref`

type
  Type defines the return type for the mutation and is a type object. Additional fields supported by :ref:`model-ref` are also supported here

  type
    Name of the type is defined in the "type" attribute, if this is not present a name will be auto-generated
  columns
    An array/object of :ref:`column-ref`

method
  Method defines what HTTP method should be used to contact the REST endpoint, if it is not defined then POST should be the default

endpoint
  Endpoint can defines a URL that the GraphQL server should make a REST action against

description
  Text description used in GraphQL schema