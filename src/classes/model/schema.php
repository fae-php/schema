<?php
/**
 * FAE 
 *
 * Copyright 2019 Callum Smith
 */
namespace FAE\schema\model;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Schema\Schema as doctrineSchema;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Types\Types;

use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Component\Cache\Exception\LogicException;

use FAE\fae\frames;
use FAE\cache\cache_adapter;

use stdClass;

class schema {
  
  var $_dbalConfig;
  var $_conn;
  var $_dataSource;
  
  var $_frames = [];
  var $_models = [];
  var $_schema;
  var $_tables;
  var $_tableData;
  var $_hooks = [];
  
  //static $_modelCache = [];
  
  const DATE_DEFAULT  = '/(?:NOW\(\)|CURRENT_TIMESTAMP|0000-00-00 00:00(?::00)?)/';
  const TYPE_TEXT     = [ Types::TEXT, Types::STRING ];
  const TYPE_NUMBER   = [ Types::INTEGER, Types::DECIMAL ];
  
  function __construct( string $dataSourceRef = 'default' )
  {
    global $dataSource;
    $this->_dbalConfig = new Configuration();
    $this->_conn = DriverManager::getConnection( $dataSource[$dataSourceRef], $this->_dbalConfig );
    $this->_dataSource = $dataSourceRef;

    // Load new doctrine types
    try {
      Type::addType('uuid', 'Ramsey\Uuid\Doctrine\UuidType');
      Type::addType('ouuid', 'Ramsey\Uuid\Doctrine\UuidBinaryOrderedTimeType');
    } catch(\Doctrine\DBAL\DBALException $e){
      // ignore redefined issues
    }

    $this->loadSchema();
  }
  
  function loadSchema( bool $cache = true )
  {
    $cacheAdapter = new cache_adapter(cache_adapter::LOW);
    $cacheInstance = $cacheAdapter->getCache();

    if ($cache) {
      $data = $cacheInstance->get('dbschema', function(ItemInterface $item){
        $item->expiresAfter(86400);
        try {
          $item->tag(['db']);
        } catch(LogicException $e){
          // ignore, as tags are not unanimously supported
        }
        return $this->loadSchemaData();
      });
    } else {
      $data = $this->loadSchemaData();
    }
    $this->_models = $data;
    return $data;
  }
  
  function clearCache(){
    $cacheAdapter = new cache_adapter(cache_adapter::LOW);
    $cacheInstance = $cacheAdapter->getCache();
    $cacheInstance->delete('dbschema');
  }
  
  function loadSchemaData(){
    
    $this->loadModelData();
    
    // Compute intensive cycle over models to build in extra data
    foreach( $this->_models as $model => $data ){
      if( !property_exists( $data, 'class' ) && property_exists( $data, 'frame' ) ){
        $this->_models[$model]->class = '\\FAE\\'.$data->frame.'\\'.$model;
      }
      foreach( $this->_models as $targetModel => $targetData ){
        if( $model != $targetModel ){
          foreach( (array) $targetData->columns as $name => $column ){
            if( property_exists( $column, 'depends' ) ){
              foreach( $column->depends as $table => $relations ){
                // Only add a child if the target table is the parent model and that it doesnt relate to a different schema
                if( $table == $model && (!property_exists($relations, 'relates')) ){
                  $this->_models[$model]->childrenList[] = $targetModel;
                  $this->_models[$model]->children[] = [ 'table' => $targetModel, 'setAction' => ( property_exists( $relations, 'setAction' ) ? $relations->setAction : null ) ];
                }
              }
            }
          }
        }
      }
      foreach( $data->columns as $name => $column ){
        if( !isset($this->_models[$model]->columnList) || !is_array($this->_models[$model]->columnList) ){
          $this->_models[$model]->columnList = [];
        }
        if( !in_array( $name, $this->_models[$model]->columnList ) ){
          $this->_models[$model]->columnList[] = $name;
        }
        // Build required columns
        if(!property_exists( $column, 'default' ) || property_exists( $column, 'notnull' )){
          $this->_models[$model]->required[] = $name;
        }
        // Build parent columns
        if( property_exists( $column, 'depends') ){
          foreach( $column->depends as $table => $relations ){
            /**
             * HMS-976 Incorporate dependencies into remote schema references
             * To ensure that the full reference (schema_name.model_name) is used in parent table references
             * the reference is set based on `relates` being present in the depends object
             */
            if(property_exists($relations, 'relates')){
              // Use remoteParents and not parents for remote schema references
              $this->_models[$model]->remoteParents[] = "{$relations->relates}.{$table}";
            } else {
              $this->_models[$model]->parents[] = $table;
            }
          }
        }
      }
    }
    
    // Load a post-hook
    foreach( $this->_frames as $frame => $options ){
      // Load models through hooks assigned to the frame
      if( property_exists( $options, 'hooks' ) && property_exists( $options->hooks, 'schema' ) && property_exists( $options->hooks->schema, 'postSchema' ) ){
        if( is_array($options->hooks->schema->postSchema) ){
          foreach( $options->hooks->schema->postSchema as $hook ){
            if( is_callable($hook) ){
              $this->_models = call_user_func( $hook, $this->_models );
            }
          }
        }
      }
    }
    
    return $this->_models;
  }
  
  function loadModelData()
  {
    $framesRef = new frames();
    $this->_frames = $framesRef->loadFrames();
    
    if(!is_array($this->_models)){
      $this->_models = [];
    }
    
    // Process models for all frames
    foreach( $this->_frames as $frame => $options ){
      
      // Load model files in the directory
      $modelPath = implode( DIRECTORY_SEPARATOR, [ $options->path, 'src', 'models' ] );
      if( is_dir($modelPath) ){
        
        $modelList = scandir($modelPath);
        foreach( $modelList as $modelFile ){
          if(!preg_match('/\.json$/', $modelFile)){
            continue;
          }
          
          $model = str_replace( '.json', '', $modelFile );
          if( in_array( $model, $this->_models ) ){
            throw new \Exception('Cannot redefine model '.$model);
          }
          if( preg_match( '/^[a-z_]+\.json/', $modelFile ) ){
            $this->_models[$model] = $this->loadModelFile( $model, $modelPath . DIRECTORY_SEPARATOR . $modelFile, false, $frame );
          }
          
        }
      }
      
      // Load models through hooks assigned to the frame
      if( is_object($options) && property_exists( $options, 'hooks' ) && property_exists( $options->hooks, 'schema' ) && property_exists( $options->hooks->schema, 'schema' ) ){
        if( is_array($options->hooks->schema->schema) ){
          foreach( $options->hooks->schema->schema as $hook ){
            if( is_callable($hook) ){
              $this->_models = call_user_func( $hook, $this->_models );
            }
          }
        }
      }
      
    }
  }
  
  function loadModel( string $model, $modelFile = '', bool $cache = true )
  {
    if( array_key_exists( $model, $this->_models ) ){
      return $this->_models[$model];
    }
    
    // If cached return
    // Proper symfony caching to come
    
    // Else build model cache and return
    $this->loadSchema();
    return ( array_key_exists( $model, $this->_models ) ? $this->_models[$model] : false );
  }
  
  function loadModelFile( string $model, string $modelFile = '', bool $cache = true, ?string $frame = null )
  { 
    if( !file_exists($modelFile) ){
      throw new \Exception("Model file does not exist for {$model}");
    }
    
    if( !( $content = file_get_contents($modelFile) ) ){
      throw new \Exception("Could not get file contents for model {$model}");
    }
    
    if( !( $data = json_decode($content) ) ){
      throw new \Exception("Could not parse JSON for model {$model}");
    }
    
    if($frame){
      $data->frame = $frame;
    }
    
    return $data;
  }
  
  function defineSchema( $model = null )
  {
    if( !$model && !$this->_models ){
      // error
      throw new \Exception('No models were defined or loaded');
    }
    
    $models = ($model ? [$model] : $this->_models);
    
    $this->_schema = new doctrineSchema();
    $this->_tables = $this->_tables ? $this->_tables : new stdClass();
    
    // Load basic table schema
    foreach( $models as $model ){
      $this->_tables->{$model->table} = $this->_schema->createTable($model->table);
      foreach( $model->columns as $name => $column ){
        $column = $this->translateColumn($column);
        $this->_tables->{$model->table}->addColumn(
          $name,
          $column->type,
          $this->columnOptions($column)
        );
        if( property_exists($column, 'primary') ){
          $this->_tables->{$model->table}->setPrimaryKey([$name]);
        }
        if( property_exists($column, 'unique') ){
          $this->_tables->{$model->table}->addUniqueIndex([$name]);
        }
        if( property_exists($column, 'index') ){
          $this->_tables->{$model->table}->addIndex([$name]);
        }
      }
      if(property_exists($model, 'indexes') && is_array($model->indexes)){
        foreach($model->indexes as $index){
          if(property_exists($index, 'unique')){
            $this->_tables->{$model->table}->addUniqueIndex($index->columns, null, $index->options ? (array) $index->options : []);
          } else {
            $this->_tables->{$model->table}->addIndex($index->columns, null, $index->flags ? (array) $index->flags : [], $index->options ? (array) $index->options : []);
          }
        }
      }
    }
    
    // Load table foreign relations
    foreach( $models as $model ){
      foreach( $model->columns as $name => $column ){
        if( property_exists( $column, 'depends' ) ){
          foreach( $column->depends as $relTable => $options){

            /**
             * HMS-976 Incorporate dependencies into remote schema references
             * If the dependency is on an external schema (as defined by `relates` option) then
             * we skip the creation of foreign-key relationships in the database
             */
            if(property_exists($options, 'relates') 

              /**
               * HMS-1507: If we EXPLICITLY define a FK relationship, allow the LOCAL foreign key to be
               * created.
               */
              && !property_exists( $options, 'column' ) 
              && !property_exists( $options, 'update' ) 
              && !property_exists( $options, 'delete' )) {
              continue;
            }
            
            $options->column = property_exists( $options, 'column' ) ? $options->column : 'id';
            $options->update = property_exists( $options, 'update' ) ? $options->update : 'RESTRICT';
            $options->delete = property_exists( $options, 'delete' ) ? $options->delete : 'RESTRICT';
            
            $this->_tables->{$model->table}->addForeignKeyConstraint(
              $relTable,
              [$name],
              [$options->column],
              [ 'onUpdate' => $options->update, 'onDelete' => $options->delete ]
            );
            
          }
        }
      }
    }
    
    // Load table data
    foreach( $models as $model ){
      if( property_exists( $model, 'data' ) && is_array($model->data) ){
        foreach( $model->data as $data ){
          
          $checkQuery = $this->_conn->query("SELECT COUNT(*) FROM `\table` WHERE `id` = '\data.id'");
          //needs implementing
          
        }
      }
    }
    
    return $this->_schema;
  }
  
  function createSql(){
    return $this->_schema->toSql($this->_conn->getDatabasePlatform());
  }
  
  function updateSql(){
    try {
      $currentSchema = $this->_conn->getSchemaManager()->createSchema();
      $sql = $currentSchema->getMigrateToSql( $this->_schema, $this->_conn->getDatabasePlatform() );
    } catch(\Exception $e){
      echo 'could not load update schema SQL:';
      echo '<pre>';
      print_r($this->_schema);
      throw $e;
    }
    return $sql;
  }
  
  function updateSchema(){
    $queries = $this->updateSql();
    
    $this->_conn->beginTransaction();
    try {
      $this->_conn->query('SET FOREIGN_KEY_CHECKS=0');
      foreach( $queries as $query ){
        $this->_conn->query($query);
      }
      $this->_conn->query('SET FOREIGN_KEY_CHECKS=1');
      $this->_conn->commit();
    } catch(\Exception $e) {
      $this->_conn->rollBack();
      throw $e;
    }
    
    // Invalidate model cache
    
    return true;
  }
  
  function cacheModels()
  {
    
  }
  
  function translateColumn( stdClass $column ){
    $column->typeRaw = $column->type;
    if( $column->typeRaw == 'text' && property_exists( $column, 'length' ) && $column->length < 260 ){
      $column->typeRaw = 'varchar';
    }
    $column->type = $this->translateColumnType($column->typeRaw);
    if($column->type == 'uri'){
      $column->unique = true;
    }
    return $column;
  }
  
  function translateColumnType( string $type )
  {
    if(preg_match('/^(?:tiny|small)int/i', $type)){
      return Types::SMALLINT;
    }
    if(preg_match('/^(?:big)?int/i', $type) || preg_match('/^id/i', $type)){
      return Types::INTEGER;
    }
    if(preg_match('/^decimal/i', $type)){
      return Types::DECIMAL;
    }
    if(preg_match('/^float/i', $type)){
      return Types::FLOAT;
    }
    if(preg_match('/^g?uid/i', $type)){
      return Types::GUID;
    }
    if(preg_match('/^varchar/i', $type)){
      return Types::STRING;
    }
    if(preg_match('/^text/i', $type)){
      return Types::TEXT;
    }
    if(preg_match('/^set/i', $type)){
      return Types::STRING;
    }
    if(preg_match('/^uri/i', $type)){
      return Types::STRING;
    }
    if(preg_match('/^bool/i', $type)){
      return Types::BOOLEAN;
    }
    if(preg_match('/^date(?!tz)/i', $type)){
      return Types::DATETIME_MUTABLE;
    }
    if(preg_match('/^date(?=tz)/i', $type)){
      return Types::DATETIMETZ_MUTABLE;
    }
    if(preg_match('/^json/i', $type)){
      return Types::JSON;
    }
    if(preg_match('/^[gu]uid/i', $type)){
      return Types::GUID;
    }
    if(preg_match('/^bin$/i', $type)){
      return Types::BINARY;
    }
    if(preg_match('/^ouuid$/i', $type)){
      return 'ouuid';
    }
    if(preg_match('/^uuid$/i', $type)){
      return 'uuid';
    }
    throw new \Exception($type.' column type not recognised');
  }
  
  function reverseColumnType( \Doctrine\DBAL\Schema\Column $column ){
    $type = $column->getType();
    if(preg_match('/^uri$/i', $column->getName()) && $type->getName() === Types::STRING){
      return 'uri';
    }
    if($type->getName() === Types::SMALLINT){
      return 'tinyint';
    }
    if($type->getName() === Types::INTEGER){
      return 'int';
    }
    if($type->getName() === Types::BIGINT){
      return 'bigint';
    }
    if($type->getName() === Types::DECIMAL){
      return 'decimal';
    }
    if($type->getName() === Types::FLOAT){
      return 'decimal';
    }
    if($type->getName() === Types::GUID){
      return 'uid';
    }
    if($type->getName() === Types::STRING){
      return 'varchar';
    }
    if($type->getName() === Types::TEXT){
      return 'text';
    }
    if($type->getName() === Types::BOOLEAN){
      return 'bool';
    }
    if($type->getName() === Types::DATETIME_MUTABLE){
      return 'date';
    }
    if($type->getName() === Types::DATETIMETZ_MUTABLE){
      return 'datetz';
    }
    if($type->getName() === Types::JSON){
      return 'json';
    }
    if($type->getName() === Types::BINARY){
      return 'bin';
    }
    if($type->getName() === 'ouuid'){
      return 'ouuid';
    }
    if($type->getName() === 'uuid'){
      return 'uuid';
    }
    throw new \Exception('Could not reverse column type for column '.$column->getName());
  }
    
  function reverseEngineer()
  {
    $sm = $this->_conn->getSchemaManager();
    $tables = $sm->listTables();
    
    $output = [];
    foreach($tables as $table){
      $output[$table->getName()] = $this->tableToObject($table);
    }
    return $output;
  }
  
  function tableToObject( \Doctrine\DBAL\Schema\Table $table )
  {
    $output = ['table' => $table->getName(), 'columns' => new \StdClass()];
    
    foreach($table->getColumns() as $column){
      $columnSchema = [
        'type' => $this->reverseColumnType($column),
      ];
      if($column->getLength() && $column->getLength() < 10000){
        $columnSchema['length'] = $column->getLength();
      }
      if($column->getPrecision()){
        $columnSchema['length'] = $column->getPrecision();
      }
      if($column->getScale()){
        $columnSchema['places'] = $column->getScale();
      }
      if($column->getDefault()){
        $columnSchema['default'] = $column->getDefault();
      }
      if(!$column->getNotnull()){
        $columnSchema['null'] = true;
      }
      if($column->getUnsigned()){
        $columnSchema['unsigned'] = true;
      }
      if($column->getAutoincrement()){
        $columnSchema['autoincrement'] = true;
      }
      $output['columns']->{$column->getName()} = (object) $columnSchema;
    }
    
    foreach($table->getIndexes() as $index){
      if($index->isPrimary()){
        foreach($index->getColumns() as $columnName){
          $output['columns']->{$columnName}->primary = true;
        }
        continue;
      }
      if(count($index->getColumns()) === 1){
        if($index->isUnique()){
          $output['columns']->{$index->getColumns()[0]}->unique = true;
        } else {
          $output['columns']->{$index->getColumns()[0]}->index = true;
        }
        continue;
      }
      $indexOutput = [
        'columns' => $index->getColumns(),
      ];
      if($index->isUnique()){
        $indexOutput['unique'] = true;
      }
      if($index->getFlags()){
        $indexOutput['flags'] = $index->getFlags();
      }
      if($index->getOptions()){
        $indexOutput['options'] = $index->getOptions();
      }
      $output['indexes'][] = (object) $indexOutput;
    }
    
    foreach($table->getForeignKeys() as $key){
      $columns = $key->getLocalColumns();
      
      foreach($columns as $column){
        $output['columns']->{$column}->depends->{$key->getForeignTableName()} = (object) [
          'column' => $key->getForeignColumns()[0],
          'update' => $key->onUpdate() ? strtolower($key->onUpdate()) : 'restrict',
          'delete' => $key->onDelete() ? strtolower($key->onDelete()) : 'restrict',
        ];
      }
    }
    
    return (object) $output;
  }
  
  function columnOptions( stdClass $column )
  {
    $options = [
      'notnull' => (property_exists($column, 'null') ? false : true),
      'autoincrement' => ($column->type == 'integer' && property_exists($column, 'autoincrement') ? true : false),
    ];
    if( property_exists( $column, 'default' ) && is_null($column->default) && !property_exists( $column, 'notnull' ) ){
      $options['notnull'] = false;
    }
    if( property_exists( $column, 'default' ) && $column->type == Types::BOOLEAN ){
      $options['default'] = ($column->default ? 1 : 0);
    } elseif( property_exists( $column, 'default' ) && !preg_match( $this::DATE_DEFAULT, strtoupper($column->default) ) ){
      $options['default'] = $column->default;
    }
    if( in_array($column->type, $this::TYPE_TEXT) ){
      $options['length'] = property_exists($column, 'length') ? $column->length : null;
    }
    if( in_array($column->type, $this::TYPE_NUMBER) ){
      $options['unsigned']  = property_exists($column, 'unsigned') ? true : false;
      $options['precision'] = (property_exists($column, 'length') ? $column->length : (property_exists($column, 'precision') ? $column->precision : 11));
      $options['scale']     = (property_exists($column, 'places') ? $column->places : (property_exists($column, 'scale') ? $column->scale : 0));
    }
    return $options;
  }
  
}