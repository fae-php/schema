<?php

/**
 * FAE 
 *
 * Copyright 2019 Callum Smith
 */

namespace FAE\schema\model;

use FAE\fae\fae;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Query\Expression\CompositeExpression;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;

use \PDO;
use \stdClass;
use \Exception;
use FAE\schema\exception\filterSQLException;

/**
 * Central data access object with getters, setters and delete
 *
 * @method array getColumnHook(array $columns, array $filter, model $instance)    This method is called as part of the get() stack and allows you to extend the $columns array
 * @method array getFilterHook(array $filter, array $columns)    This method is called as part of the get() stack and allows you to extend the $filter array
 * @method QueryBuilder getQueryHook(QueryBuilder $qb, array $filter, array $columns, model $instance)    This method is called as part of the get() stack and allows you to extend the QueryBuilder interface before the query is executed
 * 
 * @method array setDataHook(array $data, array $filter, string $type, model $instance)   This method allows you to modify the data array of a set() action
 * @method void setTransactionHook(array $data, array $filter, string $type, QueryBuilder $qb)    This method allows you to modify the QueryBuilder instance before the transaction is applied. Edits will have been commited here including the child stack if applicable
 * @method array setCallback(array $data, array $filter, string $type)    A function to allow a callback after a set action has been completed and the transaction closed. Must return $data
 * 
 * A centralised class that provides standardised getters setters and delete functions for model data. Models must be defined in the schema class from which this is extended.
 */
class model extends schema
{

  var $_table;
  var $_computed  = [];
  var $_columnRequest = [];
  var $_modelData;
  var $_columns;
  var $_dbalConfig;
  // @var Connection DBAL connection reference
  var $_conn;
  var $_colRegex = '[a-zA-Z0-9][0-9,a-z,A-Z\$\_]{0,63}';

  var $_searchCols = ['id' => 1000];

  static $_conns = [];

  const TYPE_INSERT = 'insert';
  const TYPE_UPDATE = 'update';

  /**
   * Initialise model class
   *
   * @param string $dataSourceRef String reference to the data source, defaults to 'default'. Data sources must be defined in the app configuration.
   * @param array $variables An array of variables to be set for the class instance.
   *
   * @return boolean True on success or false on failure
   */
  function __construct(string $dataSourceRef = 'default', array $variables = [])
  {
    global $dataSource;

    if (array_key_exists($dataSourceRef, self::$_conns)) {
      $this->_conn = self::$_conns[$dataSourceRef];
    } else {
      $this->_dbalConfig = new Configuration();
      $this->_conn = DriverManager::getConnection($dataSource[$dataSourceRef], $this->_dbalConfig);
      self::$_conns[$dataSourceRef] = $this->_conn;
    }

    if (method_exists($this, 'constructHook')) {
      try {
        $columns = call_user_func([$this, 'constructHook'], $variables);
      } catch (Exception $e) {
        print_r($e);
      }
    }

    if (!$this->_modelData) {
      $this->_modelData = $this->loadModel($this->_model, ($this->_modelFile ? $this->_modelFile : null));
    }
    $this->_table = $this->_modelData->table;
    if (!is_object($this->_modelData) || !property_exists($this->_modelData, 'columns')) {
      if ($_ENV['CVDB_DEBUG']) {
        var_dump($this->_modelData);
      }
      throw new \Exception('Malformed column data for model ' . $this->_model);
    }
    $this->_columns = isset($this->_modelData->columnList) ? $this->_modelData->columnList : [];
    return true;
  }

  /**
   * Get associative array of model by ID reference
   *
   * @param int|string $id ID of the row to be fetched, filters based on the 'id' column
   *
   * @return array|false Associative array of the matching table row
   */
  public function getById($id)
  {
    if ($result = $this->get(['id' => $id])) {
      return $result->fetch();
    }
    return false;
  }

  /**
   * Get associative array of model by ID reference
   *
   * @param array $filter FilterSQLFormat formatted array of filter data
   * @param array $columns Array of columns that will be returned by the select query. This will intersect against column namse defined by the model and cannot contain custom column references - these must be done via hooks.
   * @param int $start Offset integer of the start of pulled results, often used in conjunction with $limit
   * @param int $limit Total results to be selected from the table
   *
   * @return \Doctrine\DBAL\Statement|false Associative array of the matching table row
   */
  public function get(array $filter = [], array $columns = [], int $start = 0, int $limit = 0, array $sort = [])
  {
    $hooks = fae::$_hooks['schema'];

    $this->_columnRequest = $columns;
    $this->_computed = array_diff($columns, $this->_columns);
    if (!count($columns)) {
      $columns = $this->_columns;
    }

    if (is_array($hooks) && isset($hooks['modelGetColumns'])) {
      foreach ($hooks['modelGetColumns'] as $hook) {
        if (is_callable($hook)) {
          $columns = call_user_func($hook, $this, $columns, $filter);
        }
      }
    }

    if (method_exists($this, 'getColumnHook')) {
      try {
        $columns = call_user_func([$this, 'getColumnHook'], $columns, $filter, $this);
      } catch (Exception $e) {
        print_r($e);
      }
    }

    if (is_array($hooks) && isset($hooks['modelGetFilter'])) {
      foreach ($hooks['modelGetFilter'] as $hook) {
        if (is_callable($hook)) {
          $filter = call_user_func($hook, $this, $filter, $columns);
        }
      }
    }

    if (method_exists($this, 'getFilterHook')) {
      try {
        $filter = call_user_func([$this, 'getFilterHook'], $filter, $columns);
      } catch (Exception $e) {
        throw $e;
      }
    }

    // Escape columns
    foreach ($columns as $i => $column) {
      if (preg_match('/^' . $this->_colRegex . '$/', $column)) {
        $columnRefs[$i] = 't1.`' . $column . '`';
      } elseif (preg_match('/^\w+\.`' . $this->_colRegex . '`$/', $column)) {
        $columnRefs[$i] = $column;
      }
    }
    foreach ($filter as $col => $val) {
      if (in_array($col, $this->_columns)) {
        unset($filter[$col]);
        $filter["t1.`$col`"] = $val;
      }
    }

    // init a select query
    $qb = $this->_conn->createQueryBuilder()
      ->select(...$columnRefs)
      ->from('`' . $this->_table . '`', 't1');
    $qb = $this->buildFilterSQL($qb, $filter, $columns);

    if (count($sort)) {
      foreach ($sort as $col => $dir) {
        if (in_array($col, $this->_columns)) {
          $col = "t1.`$col`";
        }
        $qb->addOrderBy($col, $dir);
      }
    } elseif (isset($this->_defaultSort) && is_array($this->_defaultSort)) {
      foreach ($this->_defaultSort as $col => $dir) {
        if (in_array($col, $this->_columns)) {
          $col = "t1.`$col`";
        }
        $qb->addOrderBy($col, $dir);
      }
    }

    if ($limit > 0) {
      $qb->setMaxResults($limit);
      $qb->setFirstResult($start);
    }

    if (is_array($hooks) && isset($hooks['modelGetQuery'])) {
      foreach ($hooks['modelGetQuery'] as $hook) {
        if (is_callable($hook)) {
          $qb = call_user_func($hook, $qb, $filter, $columns, $this);
        }
      }
    }

    if (method_exists($this, 'getQueryHook')) {
      try {
        $qb = call_user_func([$this, 'getQueryHook'], $qb, $filter, $columns, $this);
      } catch (Exception $e) {
        throw $e;
      }
    }

    if (array_key_exists('search', $filter) && strlen($filter['search'])) {
      $qb->addSelect($this->buildSearchWeight($qb, $filter['search']));
      $qb->andWhere($this->buildSearchFilter($qb, $filter['search']));
      $qb->orderBy('`search_weight`', 'DESC');
    }

    try {
      $result = $qb->execute();
    } catch (Exception $e) {
      throw $e;
    }

    if (method_exists($this, 'getResultHook')) {
      try {
        $result = call_user_func([$this, 'getResultHook'], $result, $filter, $this);
      } catch (Exception $e) {
        throw $e;
      }
    }

    return $result;
  }

  /**
   * Get a count of data points in a table using a set filter
   *
   * @param array $filter FilterSQLFormat formatted array of filter data
   *
   * @return int Count of matching table rows in the database
   */
  public function count(array $filter = [])
  {
    return $this->get($filter, ['id'])->rowCount();
  }

  /**
   * Get a array of Statement objects for child results based on a filter
   *
   * @param array $filter FilterSQLFormat formatted array of filter data
   * @param array $columns Array of columns that will be returned by the select query. This will intersect against column namse defined by the model and cannot contain custom column references - these must be done via hooks.
   *
   * @return array Returns an array of Statement objects for each of the parent model's child data.
   */
  public function getChildrenData(array $filter = [], array $columns = [])
  {
    $queries = [];

    if ($this->hasChildren()) {
      foreach ($this->_modelData->children as $subTable) {

        $subTable = (array) $subTable;
        $subModel = $this->loadModel($subTable['table']);
        $class    = $subModel->class;

        if ($class) {
          try {
            $instance = new $class('default', ['_model' => $subTable['table']]);
            $queries[$subTable['table']] = $instance->get($filter, (array_key_exists($subTable['table'], $columns) ? $columns[$subTable['table']] : []));
          } catch (\ArgumentCountError $e) {
          }
        }
      }
    }
    foreach (fae::$_hooks['model']['getChildrenData'] as $hook) {
      if (is_callable($hook)) {
        $queries = array_merge($queries, call_user_func($hook, $filter, $columns, $this));
      }
    }
    return $queries;
  }

  /**
   * Expand an array of data with child data
   *
   * @param array $data An array of data loaded with fetchAll() from a {@see \Doctrine\DBAL\Statement} handler
   * @return array $data
   */
  public function expandDataset(array $data)
  {
    if ($this->hasChildren()) {
      foreach ($data as $k => $row) {
        $filter = [$this->_model . '_id' => $row['id']];
        $childrenQueries = $this->getChildrenData($filter);
        foreach ($childrenQueries as $childTable => $childQuery) {
          $data[$k][$childTable] = $childQuery->fetchAll();
        }
      }
    }
    if (is_array(fae::$_hooks['model']['expandDataset']) && count(fae::$_hooks['model']['expandDataset'])) {
      foreach (fae::$_hooks['model']['expandDataset'] as $hook) {
        if (is_callable($hook)) {
          $data = call_user_func($hook, $data, $this);
        }
      }
    }
    return $data;
  }

  /**
   * Parse a dataset based on column types for proper output
   *
   * @param array $data An array of data loaded with fetchAll() or fetch() from a {@see \Doctrine\DBAL\Statement} handler
   * @return array Data array with parsed content
   */
  public function parseDataset(array $data): array
  {
    if ($this->isArrayAssociative($data)) {
      return $this->parseItem($data);
    }
    foreach ($data as $key => $entry) {
      $data[$key] = $this->parseItem($entry);
    }
    return $data;
  }

  /**
   * Parse a dataset based on column types for proper output
   *
   * @param array $entry A data entry array with fetch() from a {@see \Doctrine\DBAL\Statement} handler
   * @return array Data entry with parsed content
   */
  public function parseItem(array $entry): array
  {
    foreach ($entry as $col => $content) {
      // Identify the column based on reference
      if (property_exists($this->_modelData->columns, $col)) {
        $column = $this->_modelData->columns->{$col};
      } elseif (property_exists($this->_modelData, 'filters') && property_exists($this->_modelData->filters, $col)) {
        $column = $this->_modelData->filters->{$col};
      } elseif (property_exists($this->_modelData, 'fields') && property_exists($this->_modelData->fields, $col)) {
        $column = $this->_modelData->fields->{$col};
      } else {
        continue;
      }
      $entry[$col] = $this->parseItemContent($content, $column->type);
    }
    return $entry;
  }

  /**
   * Parses data items based on a passed type
   *
   * @param mixed $content
   * @param string $type
   * @return void
   */
  function parseItemContent($content, ?string $type)
  {
    // Parse data based on type
    switch ($type) {
      case 'json':
        return json_decode($content??"");
      case 'bool':
        return (bool) $content;
      default:
        return $content;
    }
  }

  /**
   * Test whether an array is associative keys or just 0-indexed
   *
   * @param array $arr Array to test
   * @return boolean
   */
  protected function isArrayAssociative(array $arr): bool
  {
    if (array() === $arr) return false;
    return array_keys($arr) !== range(0, count($arr) - 1);
  }

  /**
   * Insert data into the database
   *
   * @param array $data Associative array of data where array keys are column names and data of the correct format. Arrays of data are automatically encoded into JSON before being stored. If the array key is in fact a model reference and the model is registered as a child of the current model the data will be iterated over and inserted as part of the same transaction.
   * @param array $filter Not used for insert queries and should be removed
   * @param mixed $escapeVars Either pass true to escape all variables or an array of columns that should not be escaped
   *
   * @return array $data that was parsed is returned (including hooked additions)
   */
  public function insert(array $data = [], array $filter = [], $escapeVars = true)
  {
    return $this->set($data, $filter, 'insert', $escapeVars);
  }

  /**
   * Update data entry in the database
   *
   * @param array $data Associative array of data where array keys are column names and data of the correct format. Arrays of data are automatically encoded into JSON before being stored. If the array key is in fact a model reference and the model is registered as a child of the current model the data will be iterated over and updated as part of the same transaction.
   * @param array $filter FilterSQLFormat formatted array of conditions that need to be matched for the updated data
   * @param mixed $escapeVars Either pass true to escape all variables or an array of columns that should not be escaped
   *
   * @return array $data that was parsed is returned (including hooked additions)
   */
  public function update(array $data = [], array $filter = [], $escapeVars = true)
  {
    return $this->set($data, $filter, 'update', $escapeVars);
  }

  /**
   * Set data entry in the database
   *
   * Usually this function would be used by one of the wrapping functions, either update() or insert() which set the $type variable automatically for you.
   *
   * @param array $data Associative array of data where array keys are column names and data of the correct format. Arrays of data are automatically encoded into JSON before being stored. If the array key is in fact a model reference and the model is registered as a child of the current model the data will be iterated over and updated as part of the same transaction.
   * @param array $filter FilterSQLFormat formatted array of conditions that need to be matched for the updated data
   * @param string $type Must be either self::TYPE_INSERT or self::TYPE_UPDATE
   * @param mixed $escapeVars Either pass true to escape all variables or an array of columns that should not be escaped
   *
   * @return Statement|boolean
   */
  public function set(array $data = [], array $filter = [], string $type = self::TYPE_INSERT, $escapeVars = true)
  {
    $hooks = fae::getHooks('schema');
    $stateless = (isset($data['_stateless']) && $data['_stateless'] ? true : false);

    if (is_array($hooks) && isset($hooks['modelSetInit'])) {
      foreach ($hooks['modelSetInit'] as $hook) {
        if (is_callable($hook)) {
          $data = call_user_func($hook, $this, $data, $filter);
        }
      }
    }

    // Begin a transactional statement
    try {
      $this->_conn->beginTransaction();
    } catch (Exception $e) {
      throw new Exception('Could not begin an SQL transaction while setting the data');
    }

    // Handle lack of support for NOW() or CURRENT_TIMESTAMP for default values
    if ($type === self::TYPE_INSERT) {
      foreach ($this->_modelData->columns as $name => $column) {
        if (array_key_exists($name, $data)) {
          continue;
        }
        if (property_exists($column, 'default')) {
          if (preg_match('/^(NOW\(\)|CURRENT_TIMESTAMP)/i', $column->default??"")) {
            $now = new \DateTime("now", new \DateTimezone('UTC'));
            $data[$name] = $now->format('Y-m-d H:i:s');
          }
        }
      }
    }

    if (is_array($hooks) && isset($hooks['modelSetData'])) {
      foreach ($hooks['modelSetData'] as $hook) {
        if (is_callable($hook)) {
          $data = call_user_func($hook, $data, $filter, $type, $this);
        }
      }
    }

    // Call the data hook if it exists
    if (method_exists($this, 'setDataHook')) {
      try {
        $data = call_user_func([$this, 'setDataHook'], $data, $filter, $type, $this);
      } catch (Exception $e) {
        //print_r($e);
        throw $e;
      }
    }

    if (!count($data)) {
      $this->_conn->commit();
      return true;
    }

    // Add the default set to the transaction list
    try {

      $qb = $this->_conn->createQueryBuilder();

      $qb = $this->buildUpdateSQL($qb, $type, $data, $escapeVars);
      $qb = $this->buildFilterSQL($qb, $filter);

      $result = $qb->execute();
    } catch (Exception $e) {
      //print_r($e);
      throw $e;
    }

    // Build transactions on any dependent tables
    if ($this->hasChildren()) {

      foreach ($this->_modelData->children as $subTable) {

        $subTable = (array) $subTable;

        if ($subTable['setAction'] !== null && is_array($data)) {

          $parentId = ($type === self::TYPE_UPDATE && !$data['id'] ? $filter['id'] : $data['id']);

          if (is_callable($subTable['setAction'])) {

            // call function
            call_user_func($subTable['setAction'], $this, $data, $parentId, $this->_modelData, $subTable);
          } elseif ($subTable['setAction'] === 'identifier' && array_key_exists($subTable['table'], $data)) {

            $subModel     = $this->loadModel($subTable['table']);
            $subData      = $data[$subTable['table']];
            $class        = $subModel->class;
            $subStateless = (array_key_exists('_stateless', $data) || $stateless ? true : false);

            if ($class) {

              $instance           = new $class;
              $subData['_table']  = $subTable['table'];
              $targetData         = [];

              // Delete all matched data if stateless
              if ($subStateless) {
                $instance->remove([$this->_table . '_id' => $parentId]);
              }

              // Iterate over data and check
              $i = 0;
              foreach ($subData as $key => $subDataItem) {
                if (is_numeric($key) && is_array($subDataItem)) {
                  if (!array_key_exists($this->_table . '_id', $subDataItem) && in_array($this->_table . '_id', $subModel->columnList)) {
                    $subDataItem[$this->_table . '_id'] = $parentId;
                  }
                  if (in_array('order', $subModel->columnList)) {
                    $subDataItem['order'] = $i;
                  }
                  // check required columns - ignore if requirements not met
                  foreach ($subModel->required as $col) {
                    if ($col != 'id' && (!array_key_exists($col, $subDataItem) || empty($subDataItem[$col]))) {
                      continue 2;
                    }
                  }
                  $targetData[] = $subDataItem;
                  $i++;
                }
              }

              // Remove data no longer present
              if ($type === self::TYPE_UPDATE && !$subStateless) {

                $subSearchCols = [];
                if (count($targetData)) {
                  foreach ($targetData[0] as $col => $val) {
                    if (in_array($col, $subModel->columnList)) {
                      $subSearchCols[] = $col;
                    }
                  }
                } else {
                  $subSearchCols = array('id', $this->_table . '_id');
                }

                $existing = $instance->get([$this->_table . '_id' => $parentId], $subSearchCols);
                while ($subExisting = $existing->fetch(PDO::FETCH_ASSOC)) {
                  if (!in_array($subExisting, $targetData)) {
                    $instance->remove($subExisting);
                  }
                }
              }

              // Insert new data that doesnt exist
              foreach ($targetData as $targetDataItem) {
                if (!$instance->count($targetDataItem) || $subStateless) {
                  $instance->insert($targetDataItem);
                }
              }
            }
          }
        }
      }
    }

    // Call the transaction hook if it exists
    if (method_exists($this, 'setTransactionHook')) {
      try {
        call_user_func([$this, 'setTransactionHook'], $data, $filter, $type, $qb);
      } catch (Exception $e) {
        print_r($e);
      }
    }

    // Execute the transaction
    try {
      $this->_conn->commit();
    } catch (\Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException $e) {
      $this->_conn->rollback();
      throw $e;
    } catch (Exception $e) {
      $this->_conn->rollback();
      throw $e;
    }

    // Call the set callback if it exists
    if (method_exists($this, 'setCallback')) {
      try {
        $data = call_user_func([$this, 'setCallback'], $data, $filter, $type);
      } catch (Exception $e) {
        throw $e;
      }
    }

    if ($type === self::TYPE_UPDATE) {
      return $result;
    } else {
      if ($id = $this->_conn->lastInsertId()) {
        $data['id'] = $id;
      }
      return $data;
    }
  }

  /**
   * Delete from the database
   *
   * @param array $filter FilterSQLFormat formatted array of conditions that need to be matched for the deleted data
   *
   * @return bool
   */
  function delete(array $filter = [])
  {
    $hooks = fae::$_hooks['schema'];

    if (is_array($hooks) && isset($hooks['modelDeleteFilter'])) {
      foreach ($hooks['modelDeleteFilter'] as $hook) {
        if (is_callable($hook)) {
          $filter = call_user_func($hook, $this, $filter);
        }
      }
    }

    // Begin a transactional statement
    try {
      $this->_conn->beginTransaction();
    } catch (Exception $e) {
      echo 'Could not begin an SQL transaction while setting the data';
      print_r($e);
    }

    // Create delete query
    try {
      $qb = $this->_conn->createQueryBuilder()
        ->delete("`{$this->_table}`");
      $qb = $this->buildFilterSQL($qb, $filter);
    } catch (Exception $e) {
      print_r($e);
    }

    if (is_array($hooks) && isset($hooks['modelDelete'])) {
      foreach ($hooks['modelDelete'] as $hook) {
        if (is_callable($hook)) {
          $qb = call_user_func($hook, $qb, $filter, $this);
        }
      }
    }

    // Call the transaction hook if it exists
    if (method_exists($this, 'deleteHook')) {
      try {
        $qb = call_user_func([$this, 'deleteHook'], $qb, $filter, $this);
      } catch (Exception $e) {
        print_r($e);
      }
    }

    $qb->execute();

    // Execute the transaction
    try {
      $this->_conn->commit();
    } catch (Exception $e) {
      $this->_conn->rollback();
      print_r($e);
      return false;
    }

    return true;
  }


  /**
   * Delete from the database
   *
   * This function is a reference to delete() and should be considered deprecated
   *
   * @param array $filter FilterSQLFormat formatted array of conditions that need to be matched for the deleted data
   *
   * @return bool
   */
  function remove(array $filter = [])
  {
    return $this->delete($filter);
  }

  /**
   * Check whether the current model has child tables
   *
   * @return bool
   */
  public function hasChildren()
  {
    if (property_exists($this->_modelData, 'children')) {
      return true;
    }
    return false;
  }

  /**
   * Add row return weighting. This will add a dynamic column called `search_weight` which will be based on the relative search column weights that are set within your model. It does not automatically apply ordering or filtering.
   *
   * @param QueryBuilder $qb Query builder interface of the query that you want to add search weighting to
   * @param string $search String that will be searched on
   *
   * @return bool
   */
  function buildSearchWeight(QueryBuilder $qb, string $search)
  {
    $stack = '(';
    $searchValFull = $qb->createNamedParameter($search . '%');
    $searchVal = $qb->createNamedParameter('%' . $search . '%');
    foreach ($this->_searchCols as $col => $weight) {
      if (is_numeric($weight) && preg_match('/^' . $this->_colRegex . '$/', $col) && in_array($col, $this->_columns)) {
        $stack .= 'IF(t1.`' . $col . '` LIKE ' . $searchValFull . ', ' . $weight . ', 0) + ';
        $stack .= 'IF(t1.`' . $col . '` LIKE ' . $searchVal . ', ' . round($weight / 2) . ', 0) + ';
      }
    }
    $partials = preg_split('/\s+/', $search);
    foreach ($partials as $partial) {
      $partialVal = $qb->createNamedParameter('%' . $partial . '%');
      foreach ($this->_searchCols as $col => $weight) {
        if (is_numeric($weight) && preg_match('/^' . $this->_colRegex . '$/', $col) && in_array($col, $this->_columns)) {
          $stack .= 'IF(t1.`' . $col . '` LIKE ' . $partialVal . ', ' . round($weight / 4) . ', 0) + ';
        }
      }
    }

    if (method_exists($this, 'buildSearchWeightHook')) {
      try {
        $stack = call_user_func([$this, 'buildSearchWeightHook'], $stack, $search, $qb);
      } catch (Exception $e) {
        print_r($e);
      }
    }

    return preg_replace('/\s\+\s$/', '', $stack) . ') AS `search_weight`';
  }

  /**
   * Add filtering by search. This mirrors the SQL filters as the weighting column, but excludes any unmatched rows from the data response.
   *
   * @param QueryBuilder $qb Query builder interface of the query that you want to add search weighting to
   * @param string $search String that will be searched on
   *
   * @return bool
   */
  function buildSearchFilter(QueryBuilder $qb, string $search)
  {
    $filter = new CompositeExpression(CompositeExpression::TYPE_OR);
    $searchVal = $qb->createNamedParameter('%' . $search . '%');
    foreach ($this->_searchCols as $col => $weight) {
      if (is_numeric($weight) && preg_match('/^' . $this->_colRegex . '$/', $col) && in_array($col, $this->_columns)) {
        $filter->add($qb->expr()->like('t1.`' . $col . '`', $searchVal));
      }
    }
    $partials = preg_split('/\s+/', $search);
    foreach ($partials as $partial) {
      $partialVal = $qb->createNamedParameter('%' . $partial . '%');
      foreach ($this->_searchCols as $col => $weight) {
        if (is_numeric($weight) && preg_match('/^' . $this->_colRegex . '$/', $col) && in_array($col, $this->_columns)) {
          $filter->add($qb->expr()->like('t1.`' . $col . '`', $partialVal));
        }
      }
    }

    if (method_exists($this, 'buildSearchFilterHook')) {
      try {
        $filter = call_user_func([$this, 'buildSearchFilterHook'], $filter, $search, $qb);
      } catch (Exception $e) {
        print_r($e);
      }
    }

    return $filter;
  }

  /**
   * Will automatically build the SQL required to perform either an insert or update query based on the data passed and columns.
   *
   * @param QueryBuilder $qb Query builder interface of the query that you want to build the SQL into
   * @param string $type Must either be 'insert' or 'update'
   * @param array $data Associative array of data (col => value) to be set in the database
   * @param mixed $escapeVars 
   * @param StdClass $cols
   * @param string $table Name of the table that the update is being performed against which will default to the _table internal class variable
   *
   * @return QueryBuilder
   */
  function buildUpdateSQL(QueryBuilder $qb, string $type = 'insert', array $data = [], $escapeVars = true, ?\StdClass $cols = null, ?string $table = null)
  {
    if (!$table) {
      $table = $this->_table;
    }
    if (preg_match('/' . $this->_colRegex . '/', $table)) {
      $table = '`' . $table . '`';
    }
    if (!$cols) {
      $cols = $this->_modelData->columns;
    }
    if ($type === 'update') {
      $qb->update($table);
    } else {
      $qb->insert($table);
    }

    $i = 0;

    foreach ($data as $col => $val) {
      if (property_exists($cols, $col)) {

        // Store arrays and objects as JSON
        if (is_array($val) || $val instanceof \stdClass) {
          $val = json_encode($val);
        }

        // Do some automatic value creation for defaults
        if ($val === '' || strtoupper($val??'') === 'NULL' || (empty($val) && $val !== 0 && $val !== "0")) {
          if (property_exists($this->_modelData->columns, $col) && property_exists($this->_modelData->columns->{$col}, 'default')) {
            $val = $this->_modelData->columns->{$col}->default;
          } else {
            $val = null;
          }
        }

        if ($type == 'update') {
          if ($escapeVars === true || (is_array($escapeVars) && !in_array($col, $escapeVars))) {
            $qb->set("`{$col}`", $qb->createNamedParameter($val));
          } else {
            $qb->set("`{$col}`", $val);
          }
        } else {
          if ($escapeVars === true || (is_array($escapeVars) && !in_array($col, $escapeVars))) {
            $qb->setValue("`{$col}`", $qb->createNamedParameter($val));
          } else {
            $qb->setValue("`{$col}`", $val);
          }
        }
        $i++;
      }
    }
    return $qb;
  }

  function buildFilterSQL(QueryBuilder $qb, array $filter = [], array $havingColumns = [], array $tableColumns = [], ?QueryBuilder $parentQb = null)
  {
    $where  = new CompositeExpression(CompositeExpression::TYPE_AND);
    $having = new CompositeExpression(CompositeExpression::TYPE_AND);

    foreach ($filter as $col => $val) {
      $testCol = $col;
      if (preg_match('/t[0-9]+\.`(' . $this->_colRegex . ')`/', $col, $matches)) {
        $testCol = $matches[1];
      }
      if (in_array($testCol, (count($tableColumns) ? $tableColumns : $this->_columns)) || in_array($col, (count($tableColumns) ? $tableColumns : $this->_columns))) {
        if (is_array($val)) {
          $this->buildFilterExpression($where, $qb, $col, $val, ($parentQb ? $parentQb : null));
        } else {
          $where->add($this->buildFilterComparison($qb, $col, $val, ($parentQb ? $parentQb : null)));
        }
      } elseif ($this->hasChildren() && in_array($col, $this->_modelData->childrenList) && is_array($val)) {
        $childIdCol  = $this->_table . "_id";
        $parentIdCol = "t1.`id`";
        $where->add("EXISTS (" . $this->buildChildFilter(($parentQb ? $parentQb : $qb), $val, $col, $parentIdCol, $childIdCol)->getSql() . ")");
      } elseif (in_array($testCol, $havingColumns) || in_array($col, $havingColumns)) {
        if (is_array($val)) {
          $this->buildFilterExpression($having, $qb, $col, $val, ($parentQb ? $parentQb : null));
        } else if (in_array($col, $this->_computed)) {
          $having->add($this->buildFilterComparison($qb, $col, $val, ($parentQb ? $parentQb : null)));
        }
      }
    }

    if ($where->count()) {
      $qb->where($where);
    }
    if ($having->count()) {
      $qb->having($having);
    }

    return $qb;
  }

  function buildChildFilter(QueryBuilder $qb, array $filter, string $table, string $parentIdCol, string $childIdCol)
  {
    static $sref = 1;
    $childFilter = array_merge([$parentIdCol => "s{$sref}.`" . $childIdCol . "`"], $filter);
    $subQuery = $this->_conn->createQueryBuilder()
      ->select($childIdCol)
      ->from($table, "s{$sref}");
    try {
      $this->buildFilterSQL($subQuery, $childFilter, [], array_merge([$parentIdCol], $this->_models[$table]->columnList), $qb);
    } catch (\Exception $e) {
      throw $e;
    }
    $sref++;
    return $subQuery;
  }

  function buildFilterExpression(CompositeExpression $ex, QueryBuilder $qb, string $col, array $val, ?QueryBuilder $parentQb = null)
  {
    $col = preg_match('/^' . $this->_colRegex . '$/', $col) ? "`$col`" : $col;
    if (array_key_exists('IN', $val) || array_key_exists('!IN', $val)) {
      $data = (array_key_exists('IN', $val) ? $val['IN'] : $val['!IN']);
      $not  = (array_key_exists('IN', $val) ? false : true);
      $value = ($parentQb ? $parentQb->createNamedParameter($data, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY) : $qb->createNamedParameter($data, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY));
      if ($not) {
        $ex->add($qb->expr()->notIn($col, $value));
      } else {
        $ex->add($qb->expr()->in($col, $value));
      }
      return true;
      /*** TODO: Add support for AND/OR/EXISTS and array based filters in filterSQLformat ***/
    } elseif (array_key_exists('AND', $val) || array_key_exists('!AND', $val)) {
    } elseif (array_key_exists('BETWEEN', $val)) {
      if (count($val) !== 1) {
        throw new filterSQLException("BETWEEN commands must be the only array option passed");
      }
      $data = $val['BETWEEN'];
      if (count($data) !== 2) {
        throw new filterSQLException("Exactly 2 integers must be passed to BETWEEN command");
      }
      sort($data);
      $ex->add($qb->expr()->gte($col, (int) $data[0]));
      $ex->add($qb->expr()->lte($col, (int) $data[1]));
    }
    return false;
  }

  function buildFilterComparison(QueryBuilder $qb, string $col, ?string $val = null, ?QueryBuilder $parentQb = null)
  {
    $not = false;
    $operand = null;
    $comparator = ExpressionBuilder::EQ;
    $col = (preg_match('/^' . $this->_colRegex . '$/', $col) ? "`$col`" : $col);

    if (preg_match('/^([&|])?([!])?([%><]=?)?/', $val, $matches)) {
      if (count($matches) > 2)
        $not = ($matches[2] == '!' ? true : false);
      if (count($matches) > 3)
        $operand = $matches[3];
      $comparator = ($not ? ExpressionBuilder::NEQ : ExpressionBuilder::EQ);
      $val = preg_replace('/^' . preg_quote($matches[0]) . '/', '', $val);
    }

    if ($val === 'NULL') {
      if ($not) {
        return $qb->expr()->isNotNull($col);
      } else {
        return $qb->expr()->isNull($col);
      }
    }

    if ($val === "true") {
      return $qb->expr()->eq($col, 1);
    } elseif ($val === "false") {
      return $qb->expr()->eq($col, 0);
    }

    // If it does not match a column format then escape
    if (!preg_match('/^([a-z][0-9]+\.)?`' . $this->_colRegex . '`$/', $val)) {
      $orig = $val;
      $val = ($parentQb ? $parentQb->createNamedParameter($val) : $qb->createNamedParameter($val));
    }

    // Special handling of date ranges
    if (preg_match('/^(\d{4}\-\d{1,2}\-\d{1,2}(?: \d{2}:\d{2}(?::\d{2})?)?) - (\d{4}\-\d{1,2}\-\d{1,2}(?: \d{2}:\d{2}(?::\d{2})?)?)$/', $orig, $matches)) {
      $dateRange  = new CompositeExpression(CompositeExpression::TYPE_AND);
      $dateRange->add($qb->expr()->gte($col, ($parentQb ? $parentQb->createNamedParameter($matches[1]) : $qb->createNamedParameter($matches[1]))));
      $dateRange->add($qb->expr()->lte($col, ($parentQb ? $parentQb->createNamedParameter($matches[2]) : $qb->createNamedParameter($matches[2]))));
      return $dateRange;
    }

    if ($operand === '%') {
      if ($not) {
        return $qb->expr()->notLike($col, $val);
      } else {
        return $qb->expr()->like($col, $val);
      }
    }

    switch ($operand) {
      case '>':
        $comparator = ($not ? ExpressionBuilder::LTE : ExpressionBuilder::GT);
        break;
      case '<':
        $comparator = ($not ? ExpressionBuilder::GTE : ExpressionBuilder::LT);
        break;
      case '>=':
        $comparator = ($not ? ExpressionBuilder::LT : ExpressionBuilder::GTE);
        break;
      case '<=':
        $comparator = ($not ? ExpressionBuilder::GT : ExpressionBuilder::LTE);
        break;
    }

    return $qb->expr()->comparison($col, $comparator, $val);
  }
}
