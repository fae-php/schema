<?php
/**
 * Schema cli manager for FAE
 * Copyright 2019 Callum Smith
 */
namespace FAE\schema;

use FAE\schema\model\schema;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class schemaUpdate extends Command {
  
  protected static $defaultName = 'app:update-schema';
  
  protected function configure()
  {
    $this
      ->setDescription('Update the database to the current version')
      ->setHelp('Help?')
      ->addOption('reverse-engineer', 'r', InputOption::VALUE_NONE, 'Reverse engineer database into model files (json format), optionally specify a directory into which files will be placed')
      ->addOption('output', 'o', InputOption::VALUE_REQUIRED, 'Directory to output the reverse engineered files into')
      ->addOption('update', 'u', InputOption::VALUE_NONE, 'Get the update syntax for the current model definitions')
      ->addOption('commit', 'c', InputOption::VALUE_NONE, 'Commit changes to the database immediately')
      ->addOption('empty-cache', 'e', InputOption::VALUE_NONE, 'Empty apcu cache of database')
    ;
  }
  
  protected function execute(InputInterface $input, OutputInterface $output){
    $schema = new schema();
    
    if($input->getOption('empty-cache')){
      // Clear the schema cache
      $schema->clearCache();
    }
    
    // If the update option is specified update the schema
    if($input->getOption('update')){
      $this->updateSchema($schema, $input, $output);
    }
    
    // If the reverse engineer option is specified
    if($input->getOption('reverse-engineer')){
      $this->reverseEngineer($schema, $input, $output);
    }
  }
  
  private function updateSchema(schema $schema, InputInterface $input, OutputInterface $output){
    // Load the schema from JSON/cache
    $schema->loadSchema();
    
    // Define the schema in DBAL
    $schema->defineSchema();
    
    if($input->getOption('commit')){
      // Commit SQL changes to the database
      $schema->updateSchema();
      $output->write('Changes committed to database successfully');
    } else {
      // Calculate SQL changes
      $sql = $schema->updateSql();
      
      foreach($sql as $line){
        $output->writeln($line.';');
      }
    }
  }
  
  private function reverseEngineer(schema $schema, InputInterface $input, OutputInterface $output){
    $tables = $schema->reverseEngineer();
    
    if(is_string($input->getOption('output')) && is_dir($input->getOption('output'))){
      $outputDir = $input->getOption('output');
      foreach($tables as $table => $data){
        // save table to file
        file_put_contents($outputDir.'/'.$table.'.json', json_encode($data, JSON_PRETTY_PRINT));
      }
      $output->write('Model files written successfully');
    } else {
      $output->write(json_encode($tables));
    }
  }
}