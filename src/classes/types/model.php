<?php

namespace FAE\schema\types;

use FAE\schema\model\schema;
use JsonSerializable;
use stdClass;

abstract class model implements JsonSerializable
{
  // @var string Name of the model
  protected $name;

  // @var object Object of {@see column} objects
  protected $columns;

  // @var object Object of {@see column} objects
  protected $filters;

  // @var object Object of {@see column} objects
  protected $fields;

  // @var array Array of child names
  protected $childrenList = [];

  // @var array Array of parent names
  protected $parents = [];

  // @var array Array of remote parent names
  protected $remoteParents = [];

  /**
   * Instantiate the columns and filters as stdClass objects
   */
  public function __construct()
  {
    $this->columns = new stdClass;
    $this->filters = new stdClass;
    $this->fields = new stdClass;
    if (!$this->name) {
      $this->name = get_called_class();
    }
  }

  /**
   * Automatically load column references using the model defined in the schema
   *
   * @param string $name Name of the model reference to load. Defaults to the called class name
   * @return void
   */
  public function loadFromModel(?string $name = null)
  {
    $schema = new schema();
    $model = $schema->loadModel($name ?? get_called_class());
    foreach ($model->columns as $columnName => $columnOptions) {
      $this->addColumn($columnName, (array) $columnOptions);
    }
    if (property_exists($model, 'filters')) {
      foreach ($model->filters as $filterName => $filterOptions) {
        $this->addFilter($filterName, (array) $filterOptions);
      }
    }
    if (property_exists($model, 'fields')) {
      foreach ($model->fields as $fieldName => $fieldOptions) {
        $this->addField($fieldName, (array) $fieldOptions);
      }
    }
    $this->addChildren(isset($model->childrenList) ? $model->childrenList : null);
    $this->addParents(isset($model->parents) ? $model->parents : null);
    $this->addRemoteParents(isset($model->remoteParents) ? $model->remoteParents : null);
  }

  /**
   * Add a column to the model object. This will create an array of columns in the {@see column} type format
   *
   * @param string|column Either a string reference for the column or an existing {@see column} object
   * @param array $options Options array to be passed to the {@see column} object creation
   * @return void
   */
  public function addColumn($column, array $options = []): void
  {
    if (is_string($column)) {
      $options['name'] = $column;
      $this->columns->{$column} = new column($options);
    }
    if ($column instanceof column) {
      // undecided whether to specifiy a clone here or not
      // there are benefits to both approaches
      $this->columns->{$column->__toString()} = $column;
    }

    // Build parent columns for dynamic columns
    foreach($this->columns as $column) {
      if ( !empty( $column->getDepends() ) ){ 
        foreach( $column->getDepends() as $table => $relations ){
          /**
           * HMS-1447 Incorporate dependencies into remote schema references for code-defined types
           * To ensure that the full reference (schema_name.model_name) is used in parent table references
           * the reference is set based on `relates` being present in the depends object
           */
          if(!empty($relations->relates)){
            // Use remoteParents and not parents for remote schema references
            $this->addRemoteParents(["{$relations->relates}.{$table}"]);
          } else {
            $this->addParents([$table]);
          }
        }
      }
    }
  }

  /**
   * Get a column from the model
   *
   * @param string $name Name of the column to get
   * @return column
   */
  public function getColumn(string $name): column
  {
    return $this->columns->{$name};
  }

  /**
   * Remove a column from the model
   *
   * @param string $name
   * @return void
   */
  public function removeColumn(string $name) 
  {
    unset($this->columns->{$name});
  }

  /**
   * Get an object containing all the columns in the model
   *
   * @return object
   */
  public function getColumns(): object
  {
    return $this->columns;
  }

  /**
   * Add a filter to the model object. This will create an array of filters in the {@see column} type format
   *
   * @param string|column Either a string reference for the filter or an existing {@see column} object
   * @param array $options Options array to be passed to the {@see column} object creation
   * @return void
   */
  public function addFilter($column, array $options = []): void
  {
    if (is_string($column)) {
      $options['name'] = $column;
      $this->filters->{$column} = new column($options);
    }
    if ($column instanceof column) {
      // undecided whether to specifiy a clone here or not
      // there are benefits to both approaches
      $this->filters->{$column->__toString()} = $column;
    }
  }

  /**
   * Get a filter from the model
   *
   * @param string $name Name of the filter to get
   * @return column
   */
  public function getFilter(string $name): column
  {
    return $this->filters->{$name};
  }

  /**
   * Remove a filter from the model
   *
   * @param string $name
   * @return void
   */
  public function removeFilter(string $name) 
  {
    unset($this->filters->{$name});
  }

  /**
   * Get an object containing all the filters in the model
   *
   * @return object
   */
  public function getFilters(): object
  {
    return $this->filters;
  }

  /**
   * Add a field to the model object. This will create an array of fields in the {@see column} type format
   *
   * @param string|column Either a string reference for the field or an existing {@see column} object
   * @param array $options Options array to be passed to the {@see column} object creation
   * @return void
   */
  public function addField(string $column, array $options): void
  {
    if (is_string($column)) {
      $options['name'] = $column;
      $this->fields->{$column} = new column($options);
    }
    if ($column instanceof column) {
      // undecided whether to specifiy a clone here or not
      // there are benefits to both approaches
      $this->fields->{$column->__toString()} = $column;
    }
  }

  /**
   * Get a field from the model
   *
   * @param string $name Name of the field to get
   * @return column
   */
  public function getField(string $name): column
  {
    return $this->fields->{$name};
  }

  /**
   * Remove a field from the model
   *
   * @param string $name
   * @return void
   */
  public function removeField(string $name) 
  {
    unset($this->fields->{$name});
  }

  /**
   * Get an object containing all the fields in the model
   *
   * @return object
   */
  public function getFields(): ?object
  {
    return $this->fields;
  }

  public function addParents(?array $parents)
  {
    if (is_array($parents)) {
      $this->parents = array_unique(array_merge($this->parents, $parents));
    }
  }

  public function addRemoteParents(?array $remoteParents)
  {
    if (is_array($remoteParents)) {
      $this->remoteParents = array_unique(array_merge($this->remoteParents, $remoteParents));
    }
  }

  public function getParents(): array
  {
    return $this->parents;
  }

  public function getRemoteParents(): array
  {
    return $this->remoteParents;
  }

  public function addChildren(?array $children)
  {
    if (is_array($children)) {
      $this->childrenList = array_merge($this->childrenList, $children);
    }
  }

  public function getChildrenList(): array
  {
    return $this->childrenList;
  }

  /**
   * Get an object representation of the model
   *
   * @return object Object representing model
   */
  public function jsonSerialize() :mixed
  {
    $return = new stdClass;
    $return->type = $this->__toString();
    $return->columns = $this->getColumns();
    if (count($this->getParents())) {
      $return->parents = $this->getParents();
    }
    if (count($this->getRemoteParents())) {
      $return->remoteParents = $this->getRemoteParents();
    }
    if (count($this->getChildrenList())) {
      $return->childrenList = $this->getChildrenList();
    }
    if (!empty((array) $this->getFilters())) {
      $return->filters = $this->getFilters();
    }
    if (!empty((array) $this->getFields())) {
      $return->fields = $this->getFields();
    }
    return $return;
  }

  public function __toString()
  {
    return $this->name;
  }
}
