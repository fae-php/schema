<?php

namespace FAE\schema\types;

use Doctrine\DBAL\Schema\Column as SchemaColumn;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Types\Types;
use FAE\schema\exception\schemaException;
use JsonSerializable;

class column extends SchemaColumn implements JsonSerializable
{
  // @var string
  protected $name;

  // @var string Type of column
  protected $type;

  // @var boolean Column required
  protected $required;

  // @var array Column initial data
  protected $column;

  // @var object Dependencies for the column
  protected $depends;

  // @var string Description
  protected $description;

  protected $typeDBALmap = [
    'tinyint'   => Types::SMALLINT,
    'smallint'  => Types::SMALLINT,
    'int'       => Types::INTEGER,
    'integer'   => Types::INTEGER,
    'decimal'   => Types::DECIMAL,
    'float'     => Types::FLOAT,
    'uid'       => Types::GUID,
    'guid'      => Types::GUID,
    'varchar'   => Types::STRING,
    'text'      => Types::TEXT,
    'set'       => Types::STRING,
    'uri'       => Types::STRING,
    'bool'      => Types::BOOLEAN,
    'boolean'   => Types::BOOLEAN,
    'date'      => Types::DATETIME_MUTABLE,
    'datetime'  => Types::DATETIME_MUTABLE,
    'datetz'    => Types::DATETIMETZ_MUTABLE,
    'json'      => Types::JSON,
    'binary'    => Types::BINARY,
    'ouuid'     => 'ouuid',
    'uuid'      => 'uuid',
  ];

  public function __construct(array $column = [])
  {
    // Set the column name and type
    $this->name = $column['name'];
    $this->type = $this->translateType($column['type'], $column);
    $this->depends = (isset($column['depends']) ? $column['depends'] : null);

    // Validate the name and type are correctly set
    $this->validate();

    // Define options array for instantiating the column
    $options = [
      'default'     => isset($column['default']) ? $column['default'] : null,
      'notnull'     => isset($column['notnull']) ? $column['notnull'] : null,
      'length'      => isset($column['length']) ? $column['length'] : null,
      'unsigned'    => isset($column['unsigned']) ? $column['unsigned'] : null,
      'precision'   => isset($column['precision']) ? $column['precision'] : null,
      'scale'       => isset($column['scale']) ? $column['scale'] : null,
      'fixed'       => isset($column['fixed']) ? $column['fixed'] : null,
      'comment'     => isset($column['comment']) ? $column['comment'] : null,
      'description' => isset($column['description']) ? $column['description'] : null,
    ];

    $options = $this->parseDeprecatedOptions($options, $column);

    // Instatitate a DBAL Column type
    parent::__construct($this->name, $this->getDBALType($this->type), $options);

    $this->setRequired($this->parseRequired($column));
  }

  /**
   * Retrieve access to the depends object
   *
   * @return object|null
   */
  public function getDepends() : ?object 
  {
    return (object)$this->depends;
  }

  /**
   * Set the required flag on the column
   *
   * @param boolean $required
   * @return void
   */
  public function setRequired(bool $required): void
  {
    $this->required = $required;
  }

  /**
   * Get whether the column is required or not
   *
   * @return boolean
   */
  public function getRequired(): bool
  {
    return $this->required;
  }

  /**
   * Using the column data array parse whether the column is required or not
   *
   * @param array $column
   * @return boolean
   */
  protected function parseRequired(array $column): bool
  {
    if(array_key_exists('required', $column)){
      // Support for required being parsed as a string
      if($column['required'] === "false" || !$column['required']){
        return false;
      }
      return true;
    }
    if(!$this->getDefault() && $this->getNotnull()){
      return true;
    }
    return false;
  }

  /**
   * Set the description on a column
   *
   * @param string $description
   * @return void
   */
  public function setDescription(bool $description): void
  {
    $this->description = $description;
  }

  /**
   * Get column description
   *
   * @return string|null
   */
  public function getDescription(): ?string
  {
    return (isset($this->description) ? $this->description : null);
  }

  public function getRawType(): string
  {
    return $this->type;
  }

  /**
   * Translate type automagically based on text lengths
   *
   * @deprecated v0.3 Support for varchar should not be assumed
   * @param string $type
   * @param array $column
   * @return string $type
   */
  function translateType(string $type, array $column): string
  {
    if ($type == 'text' && array_key_exists('length', $column) && $column['length'] < 260) {
      $type = 'varchar';
    }
    return $type;
  }

  public function getDBALType(): Type
  {
    if (!array_key_exists($this->type, $this->typeDBALmap)) {
      throw new schemaException("Type '{$this->type}' cannot be used as a DBAL column");
    }
    return Type::getType($this->typeDBALmap[$this->type]);
  }

  public function getSupportedTypes(): array
  {
    return array_unique(array_keys($this->typeDBALmap));
  }

  protected function validate(): void
  {
    if (!$this->name) {
      throw new schemaException("Column name not specified when defining column");
    }
    if (!$this->type) {
      throw new schemaException("Type not specified when defining column '{$this->name}'");
    }
    $this->validateType();
  }

  protected function validateType(): void
  {
    if (!array_key_exists($this->type, $this->typeDBALmap)) {
      throw new schemaException("Type '{$this->type}' is not supported");
    }
  }

  private function parseDeprecatedOptions(array $options, array $column): array
  {
    $dateDefaultRegex = '/(?:NOW\(\)|CURRENT_TIMESTAMP|0000-00-00 00:00(?::00)?)/i';
    if (is_null($options['notnull'])) {
      $options = [
        'notnull' => (array_key_exists('null', $column) ? false : true),
      ];
    }
    if (array_key_exists('default', $column) && is_null($column['default']) && !array_key_exists('notnull', $column)) {
      $options['notnull'] = false;
    }
    if (array_key_exists('default', $column) && $column['type'] == Types::BOOLEAN) {
      $options['default'] = ($column['default'] ? 1 : 0);
    } elseif (array_key_exists('default', $column) && !preg_match($dateDefaultRegex, strtoupper((string)$column['default']))) {
      $options['default'] = $column['default'];
    }
    if (in_array($column['type'], [Types::TEXT, Types::STRING])) {
      $options['length'] = array_key_exists('length', $column) ? $column['length'] : null;
    }
    if (in_array($column['type'], [Types::DECIMAL, Types::INTEGER, Types::BIGINT, Types::SMALLINT])) {
      $options['unsigned']  = array_key_exists('unsigned', $column) ? true : false;
    }
    if (in_array($column['type'], [Types::DECIMAL])) {
      $options['precision'] = array_key_exists('length', $column) ? $column['length'] : 11;
      $options['scale']     = array_key_exists('places', $column) ? $column['places'] : 0;
    }
    return $options;
  }

  /**
   * Get an array representation of the column
   *
   * @return array Array representing the column
   */
  public function jsonSerialize(): array
  {
    $return = $this->toArray();
    $return['depends'] = $this->depends;
    $return['type'] = (string) $this->getRawType();
    $return['required'] = $this->getRequired();
    if($this->getDescription()){
      $return['description'] = $this->getDescription();
    }
    return $return;
  }

  public function __toString(): string
  {
    return $this->name;
  }
}
